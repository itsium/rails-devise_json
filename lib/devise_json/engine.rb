module DeviseJson
  class Engine < ::Rails::Engine
    require "devise"
    #isolate_namespace DeviseJson

    initializer :append_migrations do |app|

      if DeviseJson::custom_model === 'DeviseJson::User'
        unless app.root.to_s.match root.to_s
          config.paths["db/migrate"].expanded.each do |expanded_path|
            app.config.paths["db/migrate"] << expanded_path
          end
        end
      end

    end
  end
end
