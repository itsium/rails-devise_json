require 'rails'
require 'devise'
require 'jpbuilder'
require "devise_json/engine"
require "devise_json/method_override"

module DeviseJson

    mattr_accessor :permitted_parameters
    @@permitted_parameters = []

    mattr_accessor :custom_model
    @@custom_model = "DeviseJson::User"


    def self.setup
        yield self
    end

    class Railtie < Rails::Railtie

        initializer "devise_json.replace_rack_methodoverride" do |app|
            Rails.logger.debug "Swapping in DeviseJson Middleware"
            app.middleware.swap Rack::MethodOverride, DeviseJson::MethodOverride
        end

    end
end
