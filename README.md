# DeviseJson

This project rocks and uses MIT-LICENSE.

## Install

 * Add to your `config/routes.rb` application file:

```ruby
mount DeviseJson::Engine, at: "", as: "devise_json"
```

 * If you don't plan to change the default `DeviseJson::User` model with your own then you can run the following command:

```shell
bundle exec rake db:migrate
```

## Configuration

 * _[Optional]_ Create file `config/initializers/devise_json.rb`:

```ruby
DeviseJson.setup do |config|

    # Add extra permitted parameters (Default to [])
    config.permitted_parameters = [:name]

    # Change default user model (Default to DeviseJson::User)
    config.custom_model = "User"

end
```