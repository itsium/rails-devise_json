#DeviseJson::Engine.routes.draw do
Rails.application.routes.draw do

    devise_for :users,
        class_name: DeviseJson::custom_model,#"DeviseJson::User",
        #:module => 'DeviseJson',
        :controllers => {
            sessions: 'sessions',
            registrations: 'registrations'
        }

    get "/users/me", :to => "users#me"

end
