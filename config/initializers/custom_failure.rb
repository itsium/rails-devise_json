class CustomFailure < Devise::FailureApp

  def respond
    if request.format == :json
      json_failure
    else
      super
    end
  end

  def json_failure
    self.content_type = "text/javascript"
    callback = ''
    callback = params[:callback] if params[:callback]
    self.response.body = (callback + '(' + {"success" => false,
                          "message" => I18n.t("devise.sessions.user.invalid"),
                          "user" => nil}.to_json + ')')
    end
end
