$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "devise_json/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "devise_json"
  s.version     = DeviseJson::VERSION
  s.authors     = "Itsium"
  s.email       = "contact@itsium.cn"
  #s.homepage    = "TODO"
  s.summary     = "JSONP authentication for devise."
  s.description = "JSONP authentication for devise."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.0.3"
  s.add_dependency "devise", "~> 3.2.3"
  s.add_dependency "jpbuilder"

  s.add_development_dependency "sqlite3"
end
