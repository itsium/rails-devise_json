class UsersController < ApplicationController
    before_filter :authenticate_user!, :except => :me

    def me
    end
end