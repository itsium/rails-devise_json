module DeviseJson
    class ApplicationController < ActionController::Base
        before_filter :configure_permitted_parameters, if: :devise_controller?
        after_filter :change_mime_type

        def change_mime_type
            if request.format == :json and params[:callback]
                response.content_type = 'text/javascript'
            end
        end

        def configure_permitted_parameters
            if DeviseJson.permitted_parameters.length > 0
                args = [:email, :password, :password_confirmation] << DeviseJson.permitted_parameters
                devise_parameter_sanitizer.for(:sign_up) { |u|
                    u.permit(*args) }
                args = [:email, :password, :password_confirmation, :current_password] << DeviseJson.permitted_parameters
                devise_parameter_sanitizer.for(:account_update) { |u|
                    u.permit(*args) }
            end
        end

        protect_from_forgery
        skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
        respond_to :json, :html
    end
end