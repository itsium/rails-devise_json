class SessionsController < Devise::SessionsController
  respond_to :json, :html

  def create
    if request.format == :json
      self.resource = warden.authenticate!(:scope => :user)
      if resource.valid_password?(params[:user][:password])
        sign_in(resource_name, resource)
        @success = true
        @user = resource
        @message = find_message(:signed_in, {})
      else
        @success = false
        @user = nil
        @message = find_message(:invalid, {})
      end
    else
      super
    end
  end

   def destroy
    if request.format == :json
      signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
      @success = true
      @message = find_message(:signed_out, {})
    else
      super
    end
  end

  # Signs in a user on sign up. You can overwrite this method in your own
  # RegistrationsController.
  def sign_up(resource_name, resource)
    sign_in(resource_name, resource)
  end

  def require_no_authentication
    super unless request.format == :json
  end
end