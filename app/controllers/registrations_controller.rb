class RegistrationsController < Devise::RegistrationsController
  respond_to :json, :html

  def create
    if request.format == :json
        build_resource(sign_up_params)

        if resource.save
          @success = true
          @user = resource
          if resource.active_for_authentication?
            sign_up(resource_name, resource)
            @message = find_message(:signed_up, {})
          else
            expire_session_data_after_sign_in!
            @message = find_message(:"signed_up_but_#{resource.inactive_message}", {})
          end
        else
          clean_up_passwords resource
          @success = false
          @user = resource
          @message = resource.errors
        end
    else
        super
    end
  end


 def update
    if request.format == :json
        self.resource = resource_class.to_adapter.get!(send(:"current_user").to_key)

        if resource.update_with_password(account_update_params)
          sign_in resource_name, resource, :bypass => true
          @success = true
          @user = resource
          @message = find_message(:updated, {})
        else
          clean_up_passwords resource
          @success = false
          @message = resource.errors
        end
    else
        super
    end
  end

  def require_no_authentication
    super unless request.format == :json
  end

end
